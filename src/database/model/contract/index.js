/**
 * @param {object} sequelize
 * @param {object} DataTypes
 * @returns {object}
 */
module.exports = (sequelize, DataTypes) => {
    const model = sequelize.define('contract', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        uuid: {
            type: DataTypes.STRING(36),
            allowNull: false,
            unique: true,
            field: 'uuid',
            defaultValue: DataTypes.UUIDV4,
        },

    }, {
        timestamps: false,
        tableName: 'contract',
    });


    // todo nakache
    // model.associate = (models) => {
    //     models.contract.belongsToMany(models.contract, { through: '?_?', foreignKey: '?_id' });
    // };

    return model;
};
