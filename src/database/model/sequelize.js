const Sequelize = require('sequelize');

const {
    database, user, password, host, port, driver,
} = require('../config/database');

/**
 * Init sequelize config with MYSQL database
 */
module.exports = new Sequelize(database, user, password, {
    host,
    port,
    dialect: driver,
    define: { timestamps: false },
    logging: Boolean(process.env.SEQUELIZE_DEBUG),
});
