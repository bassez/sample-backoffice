/**
 * @param {object} sequelize
 * @param {object} DataTypes
 * @returns {object}
 */
module.exports = (sequelize, DataTypes) => {
    const model = sequelize.define('employee', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        uuid: {
            type: DataTypes.STRING(36),
            allowNull: false,
            unique: true,
            field: 'uuid',
            defaultValue: DataTypes.UUIDV4,
        },

    }, {
        timestamps: false,
        tableName: 'employee',
    });


    // todo nakache
    // model.associate = (models) => {
    //     models.employee.belongsToMany(models.employee, { through: '?_?', foreignKey: '?_id' });
    // };

    return model;
};
