const sequelize = require('../sequelize');
const fileService = require('../../service/file');

/**
 * Get recursively all models in this directory
 * Reduce it into a beautiful object mapping models and their names
 */
const models = fileService.getFilesPathFromFolder(__dirname)
    .filter(file => (file !== module.filename) && (file.slice(-3) === '.js'))
    .reduce((loadedModels, file) => {
        const model = sequelize.import(file);
        return { ...loadedModels, [model.name]: model };
    }, {});

/**
 * Apply models associations
 */
Object.keys(models).forEach((modelName) => {
    if (models[modelName].associate) {
        models[modelName].associate(models);
    }
});

// sequelize.sync();

module.exports = models;
