/**
 * @param {object} sequelize
 * @param {object} DataTypes
 * @returns {object}
 */
module.exports = (sequelize, DataTypes) => {
    const model = sequelize.define('client', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        uuid: {
            type: DataTypes.STRING(36),
            allowNull: false,
            unique: true,
            field: 'uuid',
            defaultValue: DataTypes.UUIDV4,
        },
        idName: {
            type: DataTypes.STRING(36),
            allowNull: true,
            unique: true,
            field: 'name',
        },
        type: {
            type: DataTypes.STRING(36),
            allowNull: true,
            unique: true,
            field: 'shortName',
        },
        mark: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            unique: true,
            field: 'maxOccupants',
        },
        color: {
            type: DataTypes.STRING(36),
            allowNull: false,
            unique: true,
            field: 'basePrice',
        },

    }, {
        timestamps: false,
        tableName: 'client',
    });


    // No Need for client
    // model.associate = (models) => {
    //     models.client.belongsToMany(models.client, { through: '?_?', foreignKey: '?_id' });
    // };

    return model;
};
