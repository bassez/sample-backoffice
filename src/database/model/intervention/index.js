/**
 * @param {object} sequelize
 * @param {object} DataTypes
 * @returns {object}
 */
module.exports = (sequelize, DataTypes) => {
    const model = sequelize.define('intervention', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        uuid: {
            type: DataTypes.STRING(36),
            allowNull: false,
            unique: true,
            field: 'uuid',
            defaultValue: DataTypes.UUIDV4,
        },

    }, {
        timestamps: false,
        tableName: 'intervention',
    });


    // todo nakache
    // model.associate = (models) => {
    //     models.intervention.belongsToMany(models.intervention, { through: '?_?', foreignKey: '?_id' });
    // };

    return model;
};
