const httpStatus = require('http-status');

/**
 * Express middleware handling HttpErrors
 *
 * @param {Object} err The thrown by a controller
 * @param {Object} req
 * @param {Object} res
 * @param {Object} next
 */
module.exports = (err, req, res, next) => {
    res.status(err.code || 500).send({
        status: err.status,
        message: err.message || httpStatus['500_MESSAGE'],
    });

    return next();
};
