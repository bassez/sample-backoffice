const Joi = require('joi');
const status = require('http-status');
const HttpError = require('../error');

/**
 * Express wrapper around Joi validator
 *
 * @param {Object} schema The Joi Schema corresponding to the protected endpoint
 * @returns {Function} next An express middleware
 */
module.exports = schema => (req, res, next) => {
    const result = Joi.validate(req, schema, { allowUnknown: true, abortEarly: false });

    if (result.error) {
        const message = result.error.details.map(detail => detail.message).join(', ');
        return next(new HttpError(status.BAD_REQUEST, message));
    }

    return next();
};
