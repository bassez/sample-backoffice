// todo nakache
// const clientModel = require('../../database/model').client;

class ClientRepository {
    /**
     * Create a new sequelize instance from clientData
     * @param {object} clientData
     * @returns {Promise<object||null>} The client created
     */
    create(clientData) {
        return clientModel.create(clientData);
    }

    /**
     * update a client
     * @param {object} client
     * @param {object} clientdata
     * @returns {Promise<object||null>} The client created
     */
    update(client, clientData) {
        return client.update(clientData);
    }

    /**
     * Find client by it's uuid
     * @param {string} uuid The client uuid
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The client found
     */
    findByUuid(uuid, attributes) {
        return clientModel.findOne({
            where: { uuid },
            attributes,
        });
    }

    /**
     * Find client by it's id
     * @param {integer} id The client id
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The client found
     */
    findById(id, attributes) {
        return clientModel.findOne({
            where: { id },
            attributes,
        });
    }

    /**
     * Find client list
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The clients found
     */
    find(attributes) {
        return clientModel.findAll({
            attributes,
        });
    }

    /**
     * Destroy client
     * @param {object} client
     * @returns {Promise<object||null>} The client destroyed
     */
    delete(client) {
        return client.destroy();
    }
}

module.exports = new ClientRepository();
