// todo nakache
// const employeeModel = require('../../database/model').employee;

class EmployeeRepository {
    /**
     * Create a new sequelize instance from employeeData
     * @param {object} employeeData
     * @returns {Promise<object||null>} The employee created
     */
    create(employeeData) {
        return employeeModel.create(employeeData);
    }

    /**
     * update a employee
     * @param {object} employee
     * @param {object} employeedata
     * @returns {Promise<object||null>} The employee created
     */
    update(employee, employeeData) {
        return employee.update(employeeData);
    }

    /**
     * Find employee by it's uuid
     * @param {string} uuid The employee uuid
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The employee found
     */
    findByUuid(uuid, attributes) {
        return employeeModel.findOne({
            where: { uuid },
            attributes,
        });
    }

    /**
     * Find employee by it's id
     * @param {integer} id The employee id
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The employee found
     */
    findById(id, attributes) {
        return employeeModel.findOne({
            where: { id },
            attributes,
        });
    }

    /**
     * Find employee list
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The employees found
     */
    find(attributes) {
        return employeeModel.findAll({
            attributes,
        });
    }

    /**
     * Destroy employee
     * @param {object} employee
     * @returns {Promise<object||null>} The employee destroyed
     */
    delete(employee) {
        return employee.destroy();
    }
}

module.exports = new EmployeeRepository();
