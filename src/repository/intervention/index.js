// todo nakache
// const interventionModel = require('../../database/model').intervention;

class InterventionRepository {
    /**
     * Create a new sequelize instance from interventionData
     * @param {object} interventionData
     * @returns {Promise<object||null>} The intervention created
     */
    create(interventionData) {
        return interventionModel.create(interventionData);
    }

    /**
     * update a intervention
     * @param {object} intervention
     * @param {object} interventiondata
     * @returns {Promise<object||null>} The intervention created
     */
    update(intervention, interventionData) {
        return intervention.update(interventionData);
    }

    /**
     * Find intervention by it's uuid
     * @param {string} uuid The intervention uuid
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The intervention found
     */
    findByUuid(uuid, attributes) {
        return interventionModel.findOne({
            where: { uuid },
            attributes,
        });
    }

    /**
     * Find intervention by it's id
     * @param {integer} id The intervention id
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The intervention found
     */
    findById(id, attributes) {
        return interventionModel.findOne({
            where: { id },
            attributes,
        });
    }

    /**
     * Find intervention list
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The interventions found
     */
    find(attributes) {
        return interventionModel.findAll({
            attributes,
        });
    }

    /**
     * Destroy intervention
     * @param {object} intervention
     * @returns {Promise<object||null>} The intervention destroyed
     */
    delete(intervention) {
        return intervention.destroy();
    }
}

module.exports = new InterventionRepository();
