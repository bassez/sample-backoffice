// todo nakache
// const vehicleModel = require('../../database/model').vehicle;

class VehicleRepository {
    /**
     * Create a new sequelize instance from vehicleData
     * @param {object} vehicleData
     * @returns {Promise<object||null>} The vehicle created
     */
    create(vehicleData) {
        return vehicleModel.create(vehicleData);
    }

    /**
     * update a vehicle
     * @param {object} vehicle
     * @param {object} vehicledata
     * @returns {Promise<object||null>} The vehicle created
     */
    update(vehicle, vehicleData) {
        return vehicle.update(vehicleData);
    }

    /**
     * Find vehicle by it's uuid
     * @param {string} uuid The vehicle uuid
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The vehicle found
     */
    findByUuid(uuid, attributes) {
        return vehicleModel.findOne({
            where: { uuid },
            attributes,
        });
    }

    /**
     * Find vehicle by it's id
     * @param {integer} id The vehicle id
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The vehicle found
     */
    findById(id, attributes) {
        return vehicleModel.findOne({
            where: { id },
            attributes,
        });
    }

    /**
     * Find vehicle list
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The vehicles found
     */
    find(attributes) {
        return vehicleModel.findAll({
            attributes,
        });
    }

    /**
     * Destroy vehicle
     * @param {object} vehicle
     * @returns {Promise<object||null>} The vehicle destroyed
     */
    delete(vehicle) {
        return vehicle.destroy();
    }
}

module.exports = new VehicleRepository();
