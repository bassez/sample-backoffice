// todo nakache
// const contractModel = require('../../database/model').contract;

class ContractRepository {
    /**
     * Create a new sequelize instance from contractData
     * @param {object} contractData
     * @returns {Promise<object||null>} The contract created
     */
    create(contractData) {
        return contractModel.create(contractData);
    }

    /**
     * update a contract
     * @param {object} contract
     * @param {object} contractdata
     * @returns {Promise<object||null>} The contract created
     */
    update(contract, contractData) {
        return contract.update(contractData);
    }

    /**
     * Find contract by it's uuid
     * @param {string} uuid The contract uuid
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The contract found
     */
    findByUuid(uuid, attributes) {
        return contractModel.findOne({
            where: { uuid },
            attributes,
        });
    }

    /**
     * Find contract by it's id
     * @param {integer} id The contract id
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The contract found
     */
    findById(id, attributes) {
        return contractModel.findOne({
            where: { id },
            attributes,
        });
    }

    /**
     * Find contract list
     * @param {array<string>} attributes
     * @returns {Promise<object||null>} The contracts found
     */
    find(attributes) {
        return contractModel.findAll({
            attributes,
        });
    }

    /**
     * Destroy contract
     * @param {object} contract
     * @returns {Promise<object||null>} The contract destroyed
     */
    delete(contract) {
        return contract.destroy();
    }
}

module.exports = new ContractRepository();
