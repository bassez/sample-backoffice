const httpStatus = require('http-status');
const uuidGenerate = require('uuid');
const interventionRepository = require('../../repository/intervention');
const HttpError = require('../../error');


class InterventionController {
    constructor() {
        this.create = this.create.bind(this);
    }

    async create(req, res, next) {
        try {
            const {
                name, surname, age,
            } = req.body;

            const uuid = uuidGenerate();

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // await interventionRepository.create({
            //     uuid, name, shortName, maxOccupants, basePrice,
            // });
            // return res.status(httpStatus.CREATED).send('Intervention created successfully');


        } catch (err) {
            console.log(err);
            return next(err);
        }
    }

    async read(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const intervention = await interventionRepository.findByUuid(req.params.uuid, null);
                
                // if (!intervention) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Intervention not found'));
                // }
                // if (intervention.id) {
                //     delete intervention.id;
                // }
                // return res.status(httpStatus.OK).send(intervention);
            }

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // const interventions = await interventionRepository.find(null);
            // return res.status(httpStatus.OK).send(interventions);

        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async update(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const intervention = await interventionRepository.findByUuid(req.params.uuid, null);

                // if (!intervention) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Intervention not found'));
                // }

                // await interventionRepository.update(intervention, req.body);
                // return res.status(httpStatus.NO_CONTENT).send();
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async remove(req, res, next) {
        try {
            if (req.params.uuid) {
                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const intervention = await interventionRepository.findByUuid(req.params.uuid, null);

                // if (!intervention) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Intervention not found'));
                // }

                // await interventionRepository.delete(intervention);
                // return res.status(httpStatus.NO_CONTENT).send('Intervention deleted successfully');
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }
}

module.exports = new InterventionController();
