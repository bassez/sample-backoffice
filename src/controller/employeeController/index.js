const httpStatus = require('http-status');
const uuidGenerate = require('uuid');
const employeeRepository = require('../../repository/employee');
const HttpError = require('../../error');


class EmployeeController {
    constructor() {
        this.create = this.create.bind(this);
    }

    async create(req, res, next) {
        try {
            const {
                name, surname, age,
            } = req.body;

            const uuid = uuidGenerate();

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // await employeeRepository.create({
            //     uuid, name, shortName, maxOccupants, basePrice,
            // });
            // return res.status(httpStatus.CREATED).send('Employee created successfully');


        } catch (err) {
            console.log(err);
            return next(err);
        }
    }

    async read(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const employee = await employeeRepository.findByUuid(req.params.uuid, null);
                
                // if (!employee) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Employee not found'));
                // }
                // if (employee.id) {
                //     delete employee.id;
                // }
                // return res.status(httpStatus.OK).send(employee);
            }

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // const employees = await employeeRepository.find(null);
            // return res.status(httpStatus.OK).send(employees);

        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async update(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const employee = await employeeRepository.findByUuid(req.params.uuid, null);

                // if (!employee) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Employee not found'));
                // }

                // await employeeRepository.update(employee, req.body);
                // return res.status(httpStatus.NO_CONTENT).send();
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async remove(req, res, next) {
        try {
            if (req.params.uuid) {
                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const employee = await employeeRepository.findByUuid(req.params.uuid, null);

                // if (!employee) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Employee not found'));
                // }

                // await employeeRepository.delete(employee);
                // return res.status(httpStatus.NO_CONTENT).send('Employee deleted successfully');
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }
}

module.exports = new EmployeeController();
