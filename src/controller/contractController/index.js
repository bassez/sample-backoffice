const httpStatus = require('http-status');
const uuidGenerate = require('uuid');
const contractRepository = require('../../repository/contract');
const HttpError = require('../../error');


class ContractController {
    constructor() {
        this.create = this.create.bind(this);
    }

    async create(req, res, next) {
        try {
            const {
                name, surname, age,
            } = req.body;

            const uuid = uuidGenerate();

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // await contractRepository.create({
            //     uuid, name, shortName, maxOccupants, basePrice,
            // });
            // return res.status(httpStatus.CREATED).send('Contract created successfully');


        } catch (err) {
            console.log(err);
            return next(err);
        }
    }

    async read(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const contract = await contractRepository.findByUuid(req.params.uuid, null);
                
                // if (!contract) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Contract not found'));
                // }
                // if (contract.id) {
                //     delete contract.id;
                // }
                // return res.status(httpStatus.OK).send(contract);
            }

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // const contracts = await contractRepository.find(null);
            // return res.status(httpStatus.OK).send(contracts);

        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async update(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const contract = await contractRepository.findByUuid(req.params.uuid, null);

                // if (!contract) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Contract not found'));
                // }

                // await contractRepository.update(contract, req.body);
                // return res.status(httpStatus.NO_CONTENT).send();
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async remove(req, res, next) {
        try {
            if (req.params.uuid) {
                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const contract = await contractRepository.findByUuid(req.params.uuid, null);

                // if (!contract) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Contract not found'));
                // }

                // await contractRepository.delete(contract);
                // return res.status(httpStatus.NO_CONTENT).send('Contract deleted successfully');
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }
}

module.exports = new ContractController();
