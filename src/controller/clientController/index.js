const httpStatus = require('http-status');
const uuidGenerate = require('uuid');
const clientRepository = require('../../repository/client');
const HttpError = require('../../error');


class ClientController {
    constructor() {
        this.create = this.create.bind(this);
    }

    async create(req, res, next) {
        try {
            const {
                name, surname, age,
            } = req.body;

            const uuid = uuidGenerate();

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // await clientRepository.create({
            //     uuid, name, shortName, maxOccupants, basePrice,
            // });
            // return res.status(httpStatus.CREATED).send('Client created successfully');


        } catch (err) {
            console.log(err);
            return next(err);
        }
    }

    async read(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const client = await clientRepository.findByUuid(req.params.uuid, null);
                
                // if (!client) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Client not found'));
                // }
                // if (client.id) {
                //     delete client.id;
                // }
                // return res.status(httpStatus.OK).send(client);
            }

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // const clients = await clientRepository.find(null);
            // return res.status(httpStatus.OK).send(clients);

        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async update(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const client = await clientRepository.findByUuid(req.params.uuid, null);

                // if (!client) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Client not found'));
                // }

                // await clientRepository.update(client, req.body);
                // return res.status(httpStatus.NO_CONTENT).send();
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async remove(req, res, next) {
        try {
            if (req.params.uuid) {
                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const client = await clientRepository.findByUuid(req.params.uuid, null);

                // if (!client) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Client not found'));
                // }

                // await clientRepository.delete(client);
                // return res.status(httpStatus.NO_CONTENT).send('Client deleted successfully');
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }
}

module.exports = new ClientController();
