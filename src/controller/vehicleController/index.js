const httpStatus = require('http-status');
const uuidGenerate = require('uuid');
const vehicleRepository = require('../../repository/vehicle');
const HttpError = require('../../error');


class VehicleController {
    constructor() {
        this.create = this.create.bind(this);
    }

    async create(req, res, next) {
        try {
            const {
                name, surname, age,
            } = req.body;

            const uuid = uuidGenerate();

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // await vehicleRepository.create({
            //     uuid, name, shortName, maxOccupants, basePrice,
            // });
            // return res.status(httpStatus.CREATED).send('Vehicle created successfully');


        } catch (err) {
            console.log(err);
            return next(err);
        }
    }

    async read(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const vehicle = await vehicleRepository.findByUuid(req.params.uuid, null);
                
                // if (!vehicle) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Vehicle not found'));
                // }
                // if (vehicle.id) {
                //     delete vehicle.id;
                // }
                // return res.status(httpStatus.OK).send(vehicle);
            }

            return res.status(httpStatus.OK).send('Fake Success still whitout DB');
            //todo nakache
            // const vehicles = await vehicleRepository.find(null);
            // return res.status(httpStatus.OK).send(vehicles);

        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async update(req, res, next) {
        try {
            if (req.params.uuid) {

                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const vehicle = await vehicleRepository.findByUuid(req.params.uuid, null);

                // if (!vehicle) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Vehicle not found'));
                // }

                // await vehicleRepository.update(vehicle, req.body);
                // return res.status(httpStatus.NO_CONTENT).send();
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }


    async remove(req, res, next) {
        try {
            if (req.params.uuid) {
                return res.status(httpStatus.OK).send('Fake Success still whitout DB');
                //todo nakache
                // const vehicle = await vehicleRepository.findByUuid(req.params.uuid, null);

                // if (!vehicle) {
                //     return next(new HttpError(httpStatus.NOT_FOUND, 'Vehicle not found'));
                // }

                // await vehicleRepository.delete(vehicle);
                // return res.status(httpStatus.NO_CONTENT).send('Vehicle deleted successfully');
            }

            return res.status(httpStatus.BAD_REQUEST).send('Missing uuid parameter');
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }
}

module.exports = new VehicleController();
