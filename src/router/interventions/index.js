const express = require('express');
const router  = express.Router();
const schema  = require('./schema');
const validator = require('../../middleware/validator');
const interventionController = require('../../controller/interventionController');



router.get('/intervention/:uuid', validator(schema.read), interventionController.read);
router.get('/interventions', validator(schema.readAll), interventionController.read);
router.post('/interventions', validator(schema.create), interventionController.create);
router.put('/interventions/:uuid', validator(schema.update), interventionController.update);
router.delete('/interventions/:uuid', validator(schema.delete), interventionController.remove);

module.exports = router;