const express = require('express');
const router = express.Router();
const validator = require('../../middleware/validator');
const schema = require('./schema');
const employeeController = require('../../controller/employeeController');

router.get('/employee/:uuid', validator(schema.read), employeeController.read);
router.get('/employees', validator(schema.readAll), employeeController.read);
router.post('/employees', validator(schema.create), employeeController.create);
router.put('/employees/:uuid', validator(schema.update), employeeController.update);
router.delete('/employees/:uuid', validator(schema.delete), employeeController.remove);

module.exports = router;