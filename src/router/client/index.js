const express = require('express');
const router = express.Router();
const validator = require('../../middleware/validator');
const schema = require('./schema');
const clientController = require('../../controller/clientController');

router.get('/client/:uuid', validator(schema.read), clientController.read);
router.get('/clients', validator(schema.readAll), clientController.read);
router.post('/clients', validator(schema.create), clientController.create);
router.put('/clients/:uuid', validator(schema.update), clientController.update);
router.delete('/clients/:uuid', validator(schema.delete), clientController.remove);

module.exports = router;