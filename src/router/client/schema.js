const Joi = require('joi');

const create = Joi.object().keys({
    body: Joi.object().keys({
        companyName: Joi.string().required(),
        adress: Joi.string().required(),
        welcomeDate: Joi.string().required(),
        contactName: Joi.string().required(),
    }),
});

const read = Joi.object().keys({
    params: Joi.object().keys({
        uuid: Joi.string().required(),
    }),
});

const readAll = Joi.object().keys({
    //todo check if it can be use for filter request like search but maybe should pass it to POST
    // body: Joi.object().keys({
    // name: Joi.string().required(),
    // adress: Joi.string().required(),
    // welcomeDate: Joi.string().required(),
    // contactName: Joi.string().required(),
});

const update = Joi.object().keys({
    params: Joi.object().keys({
        uuid: Joi.string().required(),
    }),
    body: Joi.object().keys({
        name: Joi.string().required(),
        adress: Joi.string().required(),
        welcomeDate: Joi.string().required(),
        contactName: Joi.string().required(),
    }),
});

const remove = Joi.object().keys({
    params: Joi.object().keys({
        uuid: Joi.string().required(),
    }),
});


module.exports = {
    create, read, readAll, update, remove,
};
