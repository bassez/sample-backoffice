const express = require('express');
const fileManagerService = require('../service/fileManager');
const globalRouter = express.Router();

/**
 * Get recursively all routers in this directory
 */
const routers = fileManagerService.getFilesPathFromFolder(__dirname)
    .filter(v => v !== __filename)
    .filter(v => v.indexOf('index.js') !== -1)
    .map(router => require(router)); //eslint-disable-line

/**
 * Inject all routers into the global router
 */
routers.forEach(router => globalRouter.use(router));

module.exports = globalRouter;
