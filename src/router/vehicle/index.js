const express = require('express');
const router  = express.Router();
const schema  = require('./schema');
const validator = require('../../middleware/validator');
const vehicleController = require('../../controller/vehicleController');



router.get('/vehicle/:uuid', validator(schema.read), vehicleController.read);
router.get('/vehicles', validator(schema.readAll), vehicleController.read);
router.post('/vehicles', validator(schema.create), vehicleController.create);
router.put('/vehicles/:uuid', validator(schema.update), vehicleController.update);
router.delete('/vehicles/:uuid', validator(schema.delete), vehicleController.remove);

module.exports = router;