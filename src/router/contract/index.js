const express = require('express');
const router = express.Router();
const validator = require('../../middleware/validator');
const schema = require('./schema');
const contractController = require('../../controller/contractController');

router.get('/contract/:uuid', validator(schema.read), contractController.read);
router.get('/contracts', validator(schema.readAll), contractController.read);
router.post('/contracts', validator(schema.create), contractController.create);
router.put('/contracts/:uuid', validator(schema.update), contractController.update);
router.delete('/contracts/:uuid', validator(schema.delete), contractController.remove);

module.exports = router;