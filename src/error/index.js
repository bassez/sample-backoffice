const httpStatus = require('http-status');

/**
 * Error class to handle globally the error through our middleware
 */
class HttpError extends Error {
    constructor(code, message) {
        super();

        this.code = code || httpStatus.INTERNAL_SERVER_ERROR;
        this.status = httpStatus[this.code];
        this.message = message || httpStatus[`${this.code}_MESSAGE`];
        this.date = new Date();
    }
}


module.exports = HttpError;
