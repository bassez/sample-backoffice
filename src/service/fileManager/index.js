const fs = require('fs');

class FileManager {
    /**
     * Get recursively all files included in a given folder
     * @param {string} dir, Folder path
     * @param {array} filelist, list of files
     * @returns {array} A list of file path
     */
    getFilesPathFromFolder(dir, filelist = []) {
        return fs.readdirSync(dir).reduce((results, file) => {
            const filePath = `${dir}/${file}`;

            if (fs.statSync(filePath).isDirectory()) {
                return this.getFilesPathFromFolder(filePath, results);
            }

            results.push(filePath);
            return results;
        }, filelist);
    }
}

module.exports = new FileManager();
