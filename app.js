const express = require('express');
const router  = require('./src/router');
const bodyParser = require('body-parser');
const errorMiddleware = require('./src/middleware/error');
const app = express();



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(router);
app.use(errorMiddleware);

app.listen(3000, () => {
	console.log("Starting Sample back office. :3000");
})

module.exports = app;
